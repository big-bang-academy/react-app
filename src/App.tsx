import React from "react";
import "./App.css";
import UserList from "./components/UserList";
import Counter from "./components/Counter";

function App() {
  return (
    <div className="App">
      <UserList />
      <Counter />
    </div>
  );
}

export default App;
