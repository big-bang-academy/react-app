import React, { useState } from "react";
import { Button, Stack } from "@mui/material";

function Counter() {
  const [count, setCount] = useState(0);

  const handleIncrement = () => {
    setCount(count + 1);
  };

  const handleDecrement = () => {
    setCount(count - 1);
  };

  return (
    <div>
      <p>Count: {count}</p>
      <Stack spacing={2} direction="row" justifyContent="center">
        <Button variant="contained" onClick={handleIncrement}>
          Increment
        </Button>
        <Button variant="contained" onClick={handleDecrement}>
          Decrement
        </Button>
      </Stack>
    </div>
  );
}

export default Counter;
